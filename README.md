# Opdracht Browser API

# Bestanden
Download de het html bestand `index.html`
In dit bestand kan je direct de JavaScript schrijven om de opdrachten uit te voeren in de script-tag.

# Fetch API
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
Ophalen kleur via fetch() van http://gerhardboer.nl:3000
zet de achtergrondkleur van de section met de kleur uit het response

# IntersectionObserver API
https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API

1. Gebruik de IntersectionObserver API om de sections met de class color een kleur op te halen wanneer ze in beeld komen.

2. Voeg een nieuw `section` element toe aan de body via de DOM API. Geef deze de class color, en registreer het element bij de observer.

extra: haal alleen een kleur op als de section nog geen kleur heeft

# Speech API
https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis#Examples

1. stuur de kleur wat op het scherm staat naar thecolorapi. Dit is een API die je informatie over een kleur teruggeeft. 
De url is: https://www.thecolorapi.com/id?format=json&hex=${color}
bekijk de de documentatie van thecolorapi waar je uit het response de naam kunt vinden.

2. Laat de naam van de kleur uitgesproken worden door de Speech API.

extra: toon de naam van de kleur in het midden van het scherm.

# Bluetooth API
Verstuur de kleur wat op het scherm staat naar de lamp. Bekijk de slides voor wat voorbeeld code.